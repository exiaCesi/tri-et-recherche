//
// Created by Alexandre on 19/10/2018.
//

#ifndef TRI_RESEARCHALGORITHMS_H
#define TRI_RESEARCHALGORITHMS_H

#include "person.h"

int dichotomy(Person **personsToFind, Person persons[], int low, int high);

int sequential(Person **personsToFind, Person persons[], int low, int high);

#endif //TRI_RESEARCHALGORITHMS_H

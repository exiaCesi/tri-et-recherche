//
// Created by Alexandre on 17/10/2018.
//

#ifndef TRI_UTILS_H
#define TRI_UTILS_H

#include "person.h"
#include "researchAlgorithms.h"
#include <stdio.h>
#include <stdbool.h>

bool getData(Person *person, FILE **personsData);

size_t getSize(FILE **personsData);

void printDataToFile(Person *person, FILE **personsOutput);

void swap(Person *a, Person *b);

void copy(Person *a, Person *b);

int comparePersons(const Person *a, const Person *b);

int compareNameAndLastName(const Person *a, const Person *b);

int compareDistance(const Person *a, const Person *b);

int compareZipCode(const Person *a, const Person *b);

void askForSearchingPerson(Person persons[], int size);

int checkOtherLines(Person **personToFind, Person persons[], int currentIndex);

#endif //TRI_UTILS_H

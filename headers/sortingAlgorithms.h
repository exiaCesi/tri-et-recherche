//
// Created by Alexandre on 17/10/2018.
//

#ifndef TRI_SORTINGALGORITHMS_H
#define TRI_SORTINGALGORITHMS_H

#include "person.h"
#include <stdlib.h>

void bubbleSort(Person persons[], size_t size, int (*comp)(const Person *a, const Person *b));

void insertionSort(Person persons[], size_t size, int (*comp)(const Person *a, const Person *b));

void combSort(Person persons[], size_t size, int (*comp)(const Person *a, const Person *b));

void quickSort(Person persons[], int low, int high, int (*comp)(const Person *a, const Person *b));

void mergeSort(Person persons[], int low, int high, int (*comp)(const Person *a, const Person *b));

void heapSort(Person persons[], size_t size, int (*comp)(const Person *a, const Person *b));

void selectionSort(Person persons[], size_t size, int (*comp)(const Person *a, const Person *b));

void shellSort(Person persons[], size_t size, int (*comp)(const Person *a, const Person *b));

#endif //TRI_SORTINGALGORITHMS_H

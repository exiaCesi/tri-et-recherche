//
// Created by Alexandre on 17/10/2018.
//

#ifndef TRI_PERSON_H
#define TRI_PERSON_H
typedef struct Person Person;
struct Person{
    int distance;
    char prenom[25];
    char nom[25];
    int codeDepartemental;
};
#endif //TRI_PERSON_H

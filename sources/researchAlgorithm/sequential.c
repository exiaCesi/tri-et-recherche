//
// Created by Alexandre on 20/10/2018.
//

#include "../../headers/utils.h"

int sequential(Person **personsToFind, Person persons[], int low, int high) {
    int comparePersons;

    for (int i = low; i < high; i++) {
        comparePersons = compareNameAndLastName(*personsToFind, &persons[i]);
        switch (comparePersons) {
            case 0:
                personsToFind[0]->codeDepartemental = persons[i].codeDepartemental;
                personsToFind[0]->distance = persons[i].distance;
                return checkOtherLines(personsToFind, persons, i);
            default:
                continue;

        }
    }

}
//
// Created by Alexandre on 19/10/2018.
//

#include "../../headers/utils.h"
#include <stdlib.h>
#include <string.h>


int dichotomy(Person **personsToFind, Person persons[], int low, int high) {
    bool found = false;
    int milieu;
    int *comparaisonResult = calloc(1, sizeof(int));
    int size = 0;

    while (!found && ((high - low) >= 0)) {
        milieu = (low + high) / 2;
        *comparaisonResult = compareNameAndLastName(&persons[milieu], *personsToFind);

        if (*comparaisonResult == 0) {
            found = true;
            personsToFind[0]->distance = persons[milieu].distance;
            personsToFind[0]->codeDepartemental = persons[milieu].codeDepartemental;
            size = checkOtherLines(personsToFind, persons, milieu);
        } else if (*comparaisonResult == -1) {
            low = milieu + 1;
        } else {
            high = milieu - 1;
        }
    }

    if (found) {
        return size;
    } else {
        return -1;
    }

}

#include <stdio.h>
#include <stdlib.h>
#include "../headers/utils.h"
#include "../headers/sortingAlgorithms.h"
#include "../headers/researchAlgorithms.h"
#include <time.h>
#include <string.h>

int main(int argc, char *argv[]) {
    Person *persons;

    FILE *personsData = NULL;
    FILE *personsOutput = NULL;

    int (*compZipDist)(const Person *a, const Person *) = comparePersons;
    int (*compAlphabetic)(const Person *a, const Person *) = compareNameAndLastName;
    int userChoice;

    bool doingResearch = true;

    clock_t start;
    clock_t stop;
    double elapsed;

    if (argc < 3) {
        printf("Trop peu d'arguments, vous devez entrer dans l'ordre le fichier d'entr�es, et le nom du fichier de sortie");
        return -1;
    }

    size_t size = 0;

    personsData = fopen(argv[1], "r");
    personsOutput = fopen(argv[2], "w+");

    if (personsData == NULL || personsOutput == NULL)
        return -1;

    size = getSize(&personsData);

//    size = 20;

    persons = calloc(size, sizeof(Person));

    if (persons == NULL)
        return -1;

    printf("Choisissez l'algorithme de tri � utiliser (Bubble prend �norm�ment de temps) :\n1 : Bubble Sort\n2 : Insertion Sort\n3 : Merge Sort\n4 : Quick Sort\n5 : Comb Sort\n6 : Heap sort\n7 : Shell sort\n");
    scanf("%d", &userChoice);
    while (getchar() != '\n');

    for (size_t i = 0; i < size; i++)
        getData(&persons[i], &personsData);

    fclose(personsData);

    start = clock();

    switch (userChoice) {
        case 1:
            bubbleSort(persons, size, compZipDist);
            break;
        case 2:
            insertionSort(persons, size, compZipDist);
            break;
        case 3:
            mergeSort(persons, 0, size - 1, compZipDist);
            break;
        case 4:
            quickSort(persons, 0, size - 1, compZipDist);
            break;
        case 5:
            combSort(persons, size, compZipDist);
            break;
        case 6:
            heapSort(persons, size, compZipDist);
            break;
        case 7:
            shellSort(persons, size, compZipDist);
            break;
        default:
            return -1;
            break;

    }

    stop = clock();
    elapsed = (double) (stop - start) / CLOCKS_PER_SEC;

    fprintf(personsOutput, "Distance du littoral;Nom;Pr�nom;Code D�partemental\n");

    for (size_t j = 0; j < size; ++j)
        printDataToFile(&persons[j], &personsOutput);

    printf("Time elapsed: %lf\n", elapsed);

    fclose(personsOutput);

    start = clock();
    mergeSort(persons, 0, size - 1, compAlphabetic);
    stop = clock();
    elapsed = (double) (stop - start) / CLOCKS_PER_SEC;
    printf("Tri alphab�tique effectu� en : %lf secondes.\n", elapsed);

    // maxResult = 46;

    askForSearchingPerson(persons, size);

    //else
    //  printf("Personne introuvable dans la liste apr�s %lf secondes de recherche. Veuillez v�rifier l'orthographe.\n", elapsed);

    free(persons);
    persons = NULL;
    return 0;
}
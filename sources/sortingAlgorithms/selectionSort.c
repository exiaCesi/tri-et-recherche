//
// Created by Alexandre on 18/10/2018.
//

#include "../../headers/utils.h"

void selectionSort(Person persons[], size_t size, int (*comp)(Person *a, Person *b)) {
    for (int i = 0; i < size; i++) {
        int min = i;
        for (int j = i + 1; j < size + 1; j++)
            if ((*comp)(&persons[j], &persons[min]) == -1)
                min = j;
        if (min != i)
            swap(&persons[i], &persons[min]);
    }
}
//
// Created by Alexandre on 17/10/2018.
//

#include "../../headers/utils.h"
#include <stdlib.h>
#include <time.h>

int partition(Person persons[], int low, int high, int (*comp)(const Person *a, const Person *b));

int partitionMedianOfThree(Person persons[], int low, int high, int (*comp)(const Person *a, const Person *b));

void quickSort(Person persons[], int low, int high, int (*comp)(const Person *a, const Person *b)) {
    if (low < high) {
        int p = partitionMedianOfThree(persons, low, high, comp);
        quickSort(persons, low, p - 1, comp);
        quickSort(persons, p + 1, high, comp);
    }
}

int partition(Person persons[], int low, int high, int (*comp)(const Person *a, const Person *b)) {
    Person pivot = persons[high];
    int i = low;

    for (int j = low; j < high; j++) {
        if ((*comp)(&persons[j], &pivot) == -1) {
            if (i != j)
                swap(&persons[i], &persons[j]);

            i++;
        }
    }
    swap(&persons[i], &persons[high]);
    return i;
}

int partitionMedianOfThree(Person persons[], int low, int high, int (*comp)(const Person *a, const Person *b)) {
    int mid = (low + high) / 2;

    if((*comp)(&persons[mid], &persons[low]) == -1)
        swap(&persons[mid], &persons[low]);
    if((*comp)(&persons[high], &persons[low]) == -1){}
        swap(&persons[high], &persons[low]);
    if((*comp)(&persons[high], &persons[mid]) == -1)
        swap(&persons[mid], &persons[high]);

    return partition(persons, low, high, comp);
}

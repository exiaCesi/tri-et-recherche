//
// Created by Alexandre on 17/10/2018.
//

#include "../../headers/utils.h"

void insertionSort(Person persons[], size_t size, int (*comp)(const Person *a, const Person *b)) {

    for (int i = 1; i < size; i++) {
        for (int j = i; j > 0 &&
                (*comp)(&persons[j - 1], &persons[j]) == 1;
             j--) {
            swap(&persons[j], &persons[j - 1]);
        }
    }

}


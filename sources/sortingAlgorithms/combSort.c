//
// Created by Alexandre on 18/10/2018.
//

#include "../../headers/utils.h"
#include <math.h>

void combSort(Person persons[], size_t size, int (*comp)(const Person *a, const Person *b)){
    size_t intervalle = size;
    double reduceFactor = 1 / (1 - 1 / ((1 + sqrt(5)) / 2));
    bool swapped = true;

    while (intervalle > 1 || swapped == true) {
        intervalle = (size_t) (intervalle / reduceFactor);

        if(intervalle < 1)
            intervalle = 1;

        swapped = false;

        for(int i = 0; i <= size - intervalle; i++ ){
            if((*comp)(&persons[i], &persons[i+1]) == 1){
                swap(&persons[i], &persons[i+1]);
                swapped = true;
            }
        }
    }

}

//
// Created by Alexandre on 18/10/2018.
//

#include "../../headers/utils.h"

void heapify(Person persons[], size_t size, int i, int (*comp)(const Person *a, const Person *b));

void heapSort(Person persons[], size_t size, int (*comp)(const Person *a, const Person *b)){

    for (int i = (int) size / 2 - 1; i >= 0; i--)
        heapify(persons, size, i, comp);
    for (int i = (int) size - 1; i >= 0; i--) {
        swap(&persons[0], &persons[i]);
        heapify(persons, i, 0, comp);
    }
}

void heapify(Person persons[], size_t size, int i, int (*comp)(const Person *a, const Person *b)) {
    int largest = i;
    int l = 2 * i + 1;
    int r = 2 * i + 2;

    if (l < size && (*comp)(&persons[l], &persons[largest]) == 1)
        largest = l;
    if (r < size && (*comp)(&persons[r], &persons[largest]) == 1)
        largest = r;

    if (largest != i) {
        swap(&persons[i], &persons[largest]);

        heapify(persons, size, largest, comp);
    }
}
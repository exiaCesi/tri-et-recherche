//
// Created by Alexandre on 18/10/2018.
//

#include "../../headers/utils.h"
#include <stdlib.h>

void merge(Person persons[], int low, int middle, int high, int (*comp)(const Person *a, const Person *b));

void mergeSort(Person persons[], int low, int high, int (*comp)(const Person *a, const Person *b)) {
    int middle = 0;

    if (low < high) {
        middle = (low + high) / 2;
        mergeSort(persons, low, middle, comp);
        mergeSort(persons, middle + 1, high, comp);
        merge(persons, low, middle, high, comp);
    }
}

void merge(Person persons[], int low, int middle, int high, int (*comp)(const Person *a, const Person *b)) {
    int i, j, k;
    int n1 = middle - low + 1;
    int n2 = high - middle;

    Person *L = calloc(n1, sizeof(Person));
    Person *R = calloc(n2, sizeof(Person));

    for(i = 0; i < n1; i++)
        L[i] = persons[low + i];
    for(j = 0; j < n2; j++)
        R[j] = persons[middle + 1 + j];

    i = 0;
    j = 0;
    k = low;

    while(i < n1 && j < n2){
        if ((*comp)(&R[j], &L[i]) == 1){
            persons[k] = L[i];
            i++;
        }
        else{
            persons[k] = R[j];
            j++;
        }
        k++;
    }

    while(i < n1){
        persons[k] = L[i];
        i++;
        k++;
    }

    while(j < n2){
        persons[k] = R[j];
        j++;
        k++;
    }

    free(L);
    free(R);
    L = NULL;
    R = NULL;
}
//
// Created by Alexandre on 17/10/2018.
//

#include "../../headers/utils.h"

void bubbleSort(Person persons[], size_t size, int (*comp)(const Person *a, const Person *b)) {
    bool endOfSort = false;

    while (!endOfSort) {

        endOfSort = true;

        for (int i = 0; i < size - 1; ++i) {
            if ((*comp)(&persons[i], &persons[i + 1]) == 1) {
                swap(&persons[i], &persons[i + 1]);
                endOfSort = false;
            }

        }
        size--;
    }

}

//
// Created by Alexandre on 20/10/2018.
//

#include <stdlib.h>
#include "../../headers/utils.h"

void shellSort(Person persons[], size_t size, int (*comp)(const Person *a, const Person *b)) {
    int gaps[] = {238678, 103773, 45119, 19617, 8529, 3708, 1750, 701, 301, 132, 57, 23, 10, 4, 1};

    int gapsSize = sizeof(gaps) / sizeof(int);
    int k = 0;

    for (int i = 0; i < gapsSize; i++) {


        for (int j = gaps[i]; j < size; j++) {

            Person temp = persons[j];

            for (k = j; k >= gaps[i] && (*comp)(&persons[k - gaps[i]], &temp) == 1; k -= gaps[i]) {
                persons[k] = persons[k - gaps[i]];
            }

            persons[k] = temp;

        }
    }

}
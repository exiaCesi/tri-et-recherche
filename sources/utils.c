//
// Created by Alexandre on 17/10/2018.
//

#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <time.h>
#include "../headers/utils.h"
#include "../headers/sortingAlgorithms.h"
//#include "../headers/researchAlgorithms.h"

bool getData(Person *person, FILE **personsData) {

    if (*personsData != NULL) {
        fscanf(*personsData, "%d %s %s %d", &person->distance, person->nom, person->prenom, &person->codeDepartemental);
        return true;

    } else
        return false;
}

void printDataToFile(Person *person, FILE **personsOutput) {


    if (*personsOutput != NULL) {
        fprintf(*personsOutput, "%d;%s;%s;%d\n", person->distance, person->nom, person->prenom,
                person->codeDepartemental);
    }

}

size_t getSize(FILE **personsData) {
    size_t size = 0;

    fscanf(*personsData, "%llu", &size);

    return size;
}

void swap(Person *a, Person *b) {
    Person temp = *a;

    *a = *b;
    *b = temp;

}

int compareZipCode(const Person *a, const Person *b) {

    if ((a->codeDepartemental == b->codeDepartemental))
        return 0;
    else if (a->codeDepartemental > b->codeDepartemental)
        return 1;
    else
        return -1;

}

int compareDistance(const Person *a, const Person *b) {

    if (a->distance == b->distance)
        return 0;
    else if (a->distance > b->distance)
        return 1;
    else
        return -1;

}

int comparePersons(const Person *a, const Person *b) {

    if ((a->codeDepartemental == b->codeDepartemental) && (a->distance == b->distance))
        return 0;
    else if (((a->codeDepartemental == b->codeDepartemental) && (a->distance > b->distance)) ||
             (a->codeDepartemental > b->codeDepartemental))
        return 1;
    else
        return -1;

}

int compareNameAndLastName(const Person *a, const Person *b) {

    for (int i = 0; i < 25; i++) {
        char aL = a->nom[i];
        char bL = b->nom[i];
        if (aL != bL) {
            if (aL > bL)
                return 1;
            else
                return -1;
        }
    }
    for (int j = 0; j < 25; j++) {
        char aL = a->prenom[j];
        char bL = b->prenom[j];
        if (aL != bL) {
            if (aL > bL)
                return 1;
            else
                return -1;
        }
    }

    return 0;
}

void askForSearchingPerson(Person persons[], int size) {
    Person *lookingFor;
    int start, stop;
    double elapsed;

    lookingFor = calloc(1, sizeof(Person));

    bool doingResearch = true;


    while (doingResearch) {
        printf("Veuillez saisir le nom puis le pr�nom de la personne � rechercher :\n");
        scanf("%24s %24s", lookingFor->nom, lookingFor->prenom);
        while (getchar() != '\n');

        start = clock();
        int maxResult = dichotomy(&lookingFor, persons, 0, size);
        stop = clock();
        elapsed = (double) (stop - start) / CLOCKS_PER_SEC;

        //if (found)
        if (maxResult > 0) {
            switch (maxResult) {
                case 1:
                    printf("Une correspondance a �t� trouv�e :\n");
                    printf("Personne trouv�e : %d %s %s %d, en %lf secondes.\n", lookingFor[0].distance,
                           lookingFor[0].nom,
                           lookingFor[0].prenom,
                           lookingFor[0].codeDepartemental, elapsed);
                    break;
                default:
                    printf("%d correspondances :\n", maxResult);
                    for (int i = 0; i < maxResult; i++) {
                        printf("Personne trouv�e : %d %s %s %d, en %lf secondes.\n", lookingFor[i].distance,
                               lookingFor[i].nom,
                               lookingFor[i].prenom,
                               lookingFor[i].codeDepartemental, elapsed);
                    }
                    break;
            }
        } else
            printf("Aucun r�sultat trouv�, apr�s %lf secondes de recherche.\n", elapsed);

        free(lookingFor);
        lookingFor = calloc(1, sizeof(Person));

        char answer = '\0';

        do {
            printf("Souhaitez-vous faire une nouvelle recherche ? (n/y)\n");
            scanf("%c", &answer);
            while (getchar() != '\n');

            switch (answer) {
                case 'n':
                case 'N':
                    doingResearch = false;
                    break;
                case 'y':
                case 'Y':
                    doingResearch = true;
                    break;
                default:
                    printf("Veuillez r�pondre par n ou y\n");
                    break;
            }
        } while (!(answer == 'n' || answer == 'y' || answer == 'N' || answer == 'Y'));

    }
}

int checkOtherLines(Person **personToFind, Person persons[], int currentIndex) {

    bool needToDecrement = true;
    bool needToIncrement = true;
    bool isDoublon = false;
    int i = currentIndex + 1;
    int counter = 1;

    Person *personsToFindOut = calloc(1, sizeof(Person));

    personsToFindOut[0] = **personToFind;

    while (needToIncrement) {

        isDoublon = false;

        if (compareNameAndLastName(&persons[i], personToFind[0]) != 0)
            needToIncrement = false;
        else {
            for (int j = 0; j < counter; j++) {
                if (comparePersons(&persons[i], &personsToFindOut[j]) == 0) {
                    isDoublon = true;
                    i++;
                    break;
                }
            }

            if (isDoublon)
                continue;

            personsToFindOut = realloc(personsToFindOut, (counter + 1) * sizeof(Person));
            personsToFindOut[counter] = persons[i];

            counter++;
            i++;
        }
    }

    i = currentIndex - 1;

    while (needToDecrement) {

        isDoublon = false;

        if (compareNameAndLastName(&persons[i], personToFind[0]) != 0)
            needToDecrement = false;
        else {
            for (int j = 0; j < counter; j++) {
                if (comparePersons(&persons[i], &personsToFindOut[j]) == 0) {
                    isDoublon = true;
                    i--;
                    break;
                }
            }

            if (isDoublon)
                continue;
            personsToFindOut = realloc(personsToFindOut, (counter + 1) * sizeof(Person));
            personsToFindOut[counter] = persons[i];
            counter++;
            i--;
        }
    }

    shellSort(personsToFindOut, counter, comparePersons);

    *personToFind = personsToFindOut;
    return counter;
}